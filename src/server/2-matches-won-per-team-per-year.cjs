const csvParser = require('csv-parser');
const fs = require('fs');

const matchesWonPerTeamPerYear = () =>{

    const matchesWonPerTeamPerYearResultFilePath = './src/public/output/2-matches-won-per-team-per-year.json';
    const matchesCsvFilePath = './src/data/matches.csv';

    let matches = [];

    //Parsing the csv file and return result as array of objects.
    fs.createReadStream(matchesCsvFilePath)
    .pipe(csvParser())
    .on('data',(match)=>{
        return matches.push(match);
    })
    .on('end',()=>{
        writeDataToJsonFile(matchesWonPerTeamPerYearResultFilePath,JSON.stringify(matchesWonByTeamPerYearWise(matches)));
    });

    /*This function will return the season as key and the value as nested object, which contains the team name
    as key and the the number of won match as value */
    const matchesWonByTeamPerYearWise = () =>{
        let matchesWonPerTeamYearWise = matches.reduce((matchesWonTeamData,currentMatchData)=>{
            let season = currentMatchData.season;
            let winner = currentMatchData.winner;
            if(matchesWonTeamData.hasOwnProperty(winner)){
                if(matchesWonTeamData[winner][season] !== undefined){
                    matchesWonTeamData[winner][season] += 1;
                }else{
                    if(winner.trim()!=''){
                        matchesWonTeamData[winner][season] = 1;
                    }
                }
            }else{
                if(winner !==""){
                    matchesWonTeamData[winner] = {};
                }
               if(winner.trim()!= ''){
                matchesWonTeamData[winner][season] = 1;
               }
            }
            return matchesWonTeamData;
        },{});

        return matchesWonPerTeamYearWise;
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};


matchesWonPerTeamPerYear();