const csvParser = require('csv-parser');
const fs = require('fs');

const top10EconomicalBowlers2015 = () =>{

    const top10EconomicalBowlersResultFilePath = './src/public/output/4-top-10-economical-bowlers-2015.json';
    const matchesCsvFilePath = './src/data/matches.csv';
    const deliveriesCsvFilePath = './src/data/deliveries.csv';
    let matches = [];

    // parsing 2 csv files to array of objects.
    fs.createReadStream(matchesCsvFilePath)
    .pipe(csvParser())
    .on('data',(match)=>{
        return matches.push(match);
    })
    .on('end',()=>{
        const deliveries = [];
        fs.createReadStream(deliveriesCsvFilePath)
        .pipe(csvParser())
        .on('data',(delivery)=>{
            return deliveries.push(delivery);
        })
        .on('end',()=>{    
            let top10Bowler = JSON.stringify(top10EconomicalBowlers(matches,deliveries));
            writeDataToJsonFile(top10EconomicalBowlersResultFilePath,top10Bowler);   
        })
    });

    const top10EconomicalBowlers = (matches,deliveries) =>{
        //Get all the matches of season 2015
        let matchesPlayedIn2015 = matches.filter((match)=>{
            return match.season === '2015';
        });

        let bowlersStat = {};

        matchesPlayedIn2015.map((match)=>{
            deliveries.map((delivery)=>{
                //Get all the delivery of  matches played in 2015
                if(match.id === delivery.match_id){
                    let bowlerName = delivery.bowler;
                    let bowlerRunsConced = Number(delivery.total_runs)-Number(delivery.bye_runs)-Number(delivery.legbye_runs)-Number(delivery.penalty_runs);
                    if(bowlersStat.hasOwnProperty(bowlerName)){
                        bowlersStat[bowlerName]['runs'] += bowlerRunsConced;
                        //if the ball is noball or wide, don't increment the no of balls
                        if(delivery.noball_runs === '0' && delivery.wide_runs === '0')
                        {
                            bowlersStat[bowlerName]['balls'] += 1;
                            //Calculate the economy by using runs and number of balls 
                            bowlersStat[bowlerName]['economy'] = ((bowlersStat[bowlerName]['runs']/bowlersStat[bowlerName]['balls'])*6).toFixed(2);
                        }             
                    }else{
                        bowlersStat[bowlerName] = {};
                        bowlersStat[bowlerName]['runs'] = bowlerRunsConced;
                        if(delivery.noball_runs === '0' && delivery.wide_runs === '0')
                        {
                            bowlersStat[bowlerName]['balls'] = 1;
                            bowlersStat[bowlerName]['economy'] = ((bowlersStat[bowlerName]['runs']/bowlersStat[bowlerName]['balls'])*6).toFixed(2);
                        }else{
                            bowlersStat[bowlerName]['balls'] = 0;
                        }
                    }
                }
            });
        });
        //It sorted the bowler stat based on economy
        let bowlersSortedStat = Object.entries(bowlersStat).sort((firstBowlerData,secondBowlerData)=>{
            return firstBowlerData[1].economy-secondBowlerData[1].economy;
        });
        //For getting top 10 bowlers based on economy from that array
        let top10Bowler = bowlersSortedStat.slice(0,10);
       
        return Object.fromEntries(top10Bowler);
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

top10EconomicalBowlers2015();