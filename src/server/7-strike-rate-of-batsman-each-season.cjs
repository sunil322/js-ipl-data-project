const csvParser = require('csv-parser');
const fs = require('fs');

const strikeRateOfBatsmanEachSeason = () =>{

    const strikeRateOfBatsmanResultFilePath = './src/public/output/7-strike-rate-of-batsman-each-season.json';
    const matchesCsvFilePath = './src/data/matches.csv';
    const deliveriesCsvFilePath = './src/data/deliveries.csv';
    let matches = [];

    // parsing 2 csv files to array of objects.
    fs.createReadStream(matchesCsvFilePath)
    .pipe(csvParser())
    .on('data',(match)=>{
        return matches.push(match);
    })
    .on('end',()=>{
        const deliveries = [];
        fs.createReadStream(deliveriesCsvFilePath)
        .pipe(csvParser())
        .on('data',(delivery)=>{
            return deliveries.push(delivery);
        })
        .on('end',()=>{    
           let strikeRateOfBatsmanInJsonString = JSON.stringify(strikeRateOfBatsmanSeasonWise(matches,deliveries));
           writeDataToJsonFile(strikeRateOfBatsmanResultFilePath,strikeRateOfBatsmanInJsonString);
        })
    });

    
    const strikeRateOfBatsmanSeasonWise = (matches,deliveries) =>{

        //create a object store key as match id and value as match season
        let matchIdWithSeason = matches.reduce((storedMatchData,currentMatch)=>{
            storedMatchData[currentMatch.id] = currentMatch.season;            
            return storedMatchData;
        },{});

        //Get all the batsman stat season wise
        let batsmanStatWithStrikeRate = deliveries.reduce((batsmanDataPerSeason,currentDelivery)=>{

            let batsmanName = currentDelivery.batsman;
            let batsmanRuns = currentDelivery.batsman_runs;
            let season = matchIdWithSeason[currentDelivery.match_id];
            /* creating object where the batsman name as key and the value is a object, in value object
            season as key and batsman stat as value*/
            if(batsmanDataPerSeason.hasOwnProperty(batsmanName)){
                if(batsmanDataPerSeason[batsmanName][season] === undefined){
                    batsmanDataPerSeason[batsmanName][season] = {};
                    batsmanDataPerSeason[batsmanName][season]['runs'] = Number(batsmanRuns);
                    if(currentDelivery.wide_runs === '0'){
                        batsmanDataPerSeason[batsmanName][season]['balls'] = 1;
                        batsmanDataPerSeason[batsmanName][season]['strikeRate'] = ((batsmanDataPerSeason[batsmanName][season]['runs']/batsmanDataPerSeason[batsmanName][season]['balls'])*100).toFixed(2);
                    }else{
                        batsmanDataPerSeason[batsmanName][season]['balls'] = 0;
                    }
                }else{
                    batsmanDataPerSeason[batsmanName][season]['runs'] += Number(batsmanRuns);
                    if(currentDelivery.wide_runs === '0'){
                        batsmanDataPerSeason[batsmanName][season]['balls'] += 1;
                        batsmanDataPerSeason[batsmanName][season]['strikeRate'] = ((batsmanDataPerSeason[batsmanName][season]['runs']/batsmanDataPerSeason[batsmanName][season]['balls'])*100).toFixed(2);
                    }
                }
            }else{
                batsmanDataPerSeason[batsmanName] = {};
                batsmanDataPerSeason[batsmanName][season] = {};
                batsmanDataPerSeason[batsmanName][season]['runs'] = Number(batsmanRuns); 
                if(currentDelivery.wide_runs === '0'){
                    batsmanDataPerSeason[batsmanName][season]['balls'] = 1;
                    batsmanDataPerSeason[batsmanName][season]['strikeRate'] = ((batsmanDataPerSeason[batsmanName][season]['runs']/batsmanDataPerSeason[batsmanName][season]['balls'])*100).toFixed(2);
                }
            }
            return batsmanDataPerSeason;
        },{});

        let batsmanStrikeRatePerSeason = {};
        //Refactoring the object to show only the strike rate of each season with batsman name
        for(const[batsmanName,batsmanDataWithSeason] of Object.entries(batsmanStatWithStrikeRate)){
            batsmanStrikeRatePerSeason[batsmanName]= {};
            for(const [season,batsmanStat] of Object.entries(batsmanDataWithSeason)){
                batsmanStrikeRatePerSeason[batsmanName][season] = batsmanStat.strikeRate;
            }
        }
        return batsmanStrikeRatePerSeason;
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

strikeRateOfBatsmanEachSeason();