const csvParser = require('csv-parser');
const fs = require('fs');

const teamWonTossOwnMatch = () =>{

    const teamWonTossWonMatchResultFilePath = './src/public/output/5-team-won-toss-won-match.json';
    const matchesCsvFilePath = './src/data/matches.csv';

    let matches = [];

    //Parsing the csv file and return result as array of objects.
    fs.createReadStream(matchesCsvFilePath)
    .pipe(csvParser())
    .on('data',(match)=>{
        return matches.push(match);
    })
    .on('end',()=>{
        writeDataToJsonFile(teamWonTossWonMatchResultFilePath,JSON.stringify(findTeamWonTossOwnMatch(matches)));
    });

    /*The function is returning the object which contains the key as team
    and the number of matches when the team who won the toss and also won that match.*/
    const findTeamWonTossOwnMatch = (matches) =>{
        let teamStat = matches.reduce((teamWonTossOwnMatchData,currentMatchData)=>{
            let tossWinner = currentMatchData.toss_winner;
            let winner = currentMatchData.winner;
            if(tossWinner === winner){
                if(teamWonTossOwnMatchData.hasOwnProperty(winner)){
                    teamWonTossOwnMatchData[winner] += 1;
                }else{
                    teamWonTossOwnMatchData[winner] = 1;
                }
            }
            return teamWonTossOwnMatchData;
        },{})
        return teamStat;
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

teamWonTossOwnMatch();