const csvParser = require('csv-parser');
const fs = require('fs');

const highestPlayerOfMatchEachSeason = () =>{

    const resultFilePath = './src/public/output/6-highest-player-of-match-each-season.json';
    const matchesCsvFilePath = './src/data/matches.csv';

    let matches = [];

    //Parsing the csv file and return result as array of objects.
    fs.createReadStream(matchesCsvFilePath)
    .pipe(csvParser())
    .on('data',(match)=>{
        return matches.push(match);
    })
    .on('end',()=>{
        writeDataToJsonFile(resultFilePath,JSON.stringify(highestTimePlayerOfMatch(matches)));
    });

    const highestTimePlayerOfMatch = (matches) =>{
        /*Storing all the record of player of match season wise, it returns player name as key with 
        number of time player of match as value.*/
        let allPlayerOfMatchData = matches.reduce((playerOfMatchSeasonWise,currentMatch)=>{
            let season = currentMatch.season;
            let playerName = currentMatch.player_of_match;
            if(playerOfMatchSeasonWise.hasOwnProperty(season)){
                if(playerOfMatchSeasonWise[season][playerName] !== undefined){
                    playerOfMatchSeasonWise[season][playerName] += 1;
                }else{
                    playerOfMatchSeasonWise[season][playerName] = 1;
                }
            }else{
                playerOfMatchSeasonWise[season] = {};
                playerOfMatchSeasonWise[season][playerName] = 1;
            }
        return playerOfMatchSeasonWise;
        },{});
        
        let topPlayerSeasonWise = {};

        for(const[season,playerOfMatchDataSeasonWise] of Object.entries(allPlayerOfMatchData)){
            topPlayerSeasonWise[season] = {};
            //sorting the data season wise, 1st index in player data contain the times of player of the match
            let sortedPlayerOfMatchData=Object.entries(playerOfMatchDataSeasonWise).sort((firstPlayerData,secondPlayerData)=>{
                return secondPlayerData[1]-firstPlayerData[1];
            });
            //We sort the data in descending order,so 0 index contain the player data having maximum awards
            const [batsman,timesOfPlayerOfMatch] = sortedPlayerOfMatchData[0];
            topPlayerSeasonWise[season]['player'] = batsman;
            topPlayerSeasonWise[season]['times_of_player_of_match'] = timesOfPlayerOfMatch; 
        }
        return topPlayerSeasonWise;
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

highestPlayerOfMatchEachSeason();