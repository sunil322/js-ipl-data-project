const csvParser = require('csv-parser');
const fs = require('fs');

const extraRunsConcededPerTeam2016 = () =>{

    const extraRunsConcededPerTeamResultFilePath = './src/public/output/3-extra-runs-conceded-per-team-2016.json';
    const matchesCsvFilePath = './src/data/matches.csv';
    const deliveriesCsvFilePath = './src/data/deliveries.csv';
    let matches = [];

    // parsing 2 csv files to array of objects.
    fs.createReadStream(matchesCsvFilePath)
    .pipe(csvParser())
    .on('data',(match)=>{
        return matches.push(match);
    })
    .on('end',()=>{
        const deliveries = [];
        fs.createReadStream(deliveriesCsvFilePath)
        .pipe(csvParser())
        .on('data',(delivery)=>{
            return deliveries.push(delivery);
        })
        .on('end',()=>{    
            let extraRunsPerTeam = JSON.stringify(extraRunsConcededPerTeam(matches,deliveries));
            writeDataToJsonFile(extraRunsConcededPerTeamResultFilePath,extraRunsPerTeam);   
        });
    });

    //This function will return the team name as key and the total extra runs as value.
    const extraRunsConcededPerTeam = (matches,deliveries) =>{

        //Stored all the matches played in 2016.
        let matchesPlayedIn2016 = matches.filter((match)=>{
            return match.season === '2016';
        });
    
        let extraRunsPerTeam = {};
       
        matchesPlayedIn2016.map((match)=>{
            deliveries.map((delivery)=>{
                //Comparing the id for getting all the deliveries data of 2016. 
                if(match.id === delivery.match_id){
                    if(extraRunsPerTeam.hasOwnProperty(delivery.bowling_team)){
                        extraRunsPerTeam[delivery.bowling_team] += Number(delivery.extra_runs);
                    }else{
                        extraRunsPerTeam[delivery.bowling_team] = Number(delivery.extra_runs);
                    }
                }
            });
        });
        return extraRunsPerTeam;
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

extraRunsConcededPerTeam2016();