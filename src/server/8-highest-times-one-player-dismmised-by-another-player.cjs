const csvParser = require('csv-parser');
const fs = require('fs');

const highestTimesOnePlayerDismissedByAnotherPlayer = () =>{

    const deliveriesCsvFilePath = './src/data/deliveries.csv';
    const resultFilePath = './src/public/output/8-highest-times-one-player-dismmised-by-another-player.json';

    let deliveries = [];
    fs.createReadStream(deliveriesCsvFilePath)
    .pipe(csvParser())
    .on('data',(delivery)=>{
        return deliveries.push(delivery);
    })
    .on('end',()=>{
       writeDataToJsonFile(resultFilePath,JSON.stringify(highestTimePlayerDismissal(deliveries)));
    });

    const highestTimePlayerDismissal = (deliveries) =>{
        let dismissalData = {};
        deliveries.map((currentDelivery)=>{
            if(currentDelivery.player_dismissed !== '' && currentDelivery.dismissal_kind !=='run out'){
                /*Creating a key with batsman and bowler, if not present.
                If present then incrementing the value based on condition*/
                if(dismissalData.hasOwnProperty(currentDelivery.batsman+"|"+currentDelivery.bowler)){
                    dismissalData[currentDelivery.batsman+"|"+currentDelivery.bowler] += 1;
                }else{
                    dismissalData[currentDelivery.batsman+"|"+currentDelivery.bowler] = 1;
                }
            }
        })

        //Sorted the data by using times of dismissal present in 1st index 
        let sortedData = Object.entries((dismissalData)).sort((firstData,secondData)=>{
            return secondData[1]-firstData[1];
        });
        
        //Store the first data of sorted array, splitting to get batsman and bowler name
        let [batsmanName,bowlerName]=(sortedData[0][0]).split("|");
        let playerDismissalData = {};

        playerDismissalData['batsman'] = batsmanName;
        playerDismissalData['bowler'] = bowlerName; 
        playerDismissalData['times_of_dismissal'] = sortedData[0][1];
        
        return playerDismissalData;
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

highestTimesOnePlayerDismissedByAnotherPlayer();