const csvParser = require('csv-parser');
const fs = require('fs');

const matchesPerYear = () => {

    const matchesPerYearResultFilePath = './src/public/output/1-matches-per-year.json';
    const matchesCsvFilePath = './src/data/matches.csv';

    let matches = [];

    //Parsing the csv file and return result as array of objects.
    fs.createReadStream(matchesCsvFilePath)
    .pipe(csvParser())
    .on('data',(match)=>{
        return matches.push(match);
    })
    .on('end',()=>{
        writeDataToJsonFile(matchesPerYearResultFilePath,JSON.stringify(matchesPlayedPerYear(matches)));
    });

    //Return key as season and number of matches played as value in a object.
    const matchesPlayedPerYear = (matches) =>{
        let matchesPlayedPerYear = matches.reduce((matchPlayedData,currentMatchData)=>{

            let season = currentMatchData.season;

            if(matchPlayedData.hasOwnProperty(season)){
                matchPlayedData[season] += 1;
            }else{
                matchPlayedData[season] = 1;
            }
            return matchPlayedData;
        },{});
        return matchesPlayedPerYear;
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

matchesPerYear();