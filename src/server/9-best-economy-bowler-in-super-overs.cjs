const csvParser = require('csv-parser');
const fs = require('fs');

const bestconomyBowlerInSuperOvers = () =>{
    
    const deliveriesCsvFilePath = './src/data/deliveries.csv';
    const resultFilePath = './src/public/output/9-best-economy-bowler-in-super-overs.json';

    let deliveries = [];
    fs.createReadStream(deliveriesCsvFilePath)
    .pipe(csvParser())
    .on('data',(delivery)=>{
        return deliveries.push(delivery);
    })
    .on('end',()=>{
        writeDataToJsonFile(resultFilePath,JSON.stringify(economicalBowlerInSuperOvers(deliveries)));
    });

    const economicalBowlerInSuperOvers = (deliveries) =>{

        let bowlerStatInSuperOver = deliveries.reduce((bowlersStat,currentDelivery)=>{
            if(currentDelivery.is_super_over === '1'){
                let bowler = currentDelivery.bowler;
                let noballRuns = currentDelivery.noball_runs;
                let wideRuns = currentDelivery.wide_runs;
                let totalRuns = Number(currentDelivery.total_runs)-Number(currentDelivery.legbye_runs)-Number(currentDelivery.penalty_runs)-Number(currentDelivery.bye_runs);

                /*Creating object for bowler with key as bowler name and value as nested object,
                where the value object contains the balls,runs and economy as property.*/
                if(bowlersStat.hasOwnProperty(bowler)){
                    bowlersStat[bowler]['runs'] += totalRuns;
                    if(noballRuns === '0' && wideRuns === '0'){
                        bowlersStat[bowler]['balls'] += 1;
                        bowlersStat[bowler]['economy'] = ((bowlersStat[bowler]['runs'] / bowlersStat[bowler]['balls'])*6).toFixed(2);
                    } 
                }else{
                     bowlersStat[bowler] = {};
                     bowlersStat[bowler]['runs'] = totalRuns;
                     if(noballRuns === '0' && wideRuns === '0'){
                         bowlersStat[bowler]['balls'] = 1;
                         bowlersStat[bowler]['economy'] = ((bowlersStat[bowler]['runs'] / bowlersStat[bowler]['balls'])*6).toFixed(2);
                     }else{
                         bowlersStat[bowler]['balls'] = 0;
                     }
                 }
            }
            return bowlersStat;
        },{});
        //Sorting the bowler stat based on economy, 1st index contain a nested object where economy is a key.
        let sortedData = Object.entries(bowlerStatInSuperOver).sort((firstBowlerStat,secondBolwerStat)=>{
            return firstBowlerStat[1].economy - secondBolwerStat[1].economy;
        });
        
        return sortedData[0];
    };

    //Function for writing the result to the json file.
    const writeDataToJsonFile = (jsonWriteFilePath,resultData) =>{
        fs.writeFile(jsonWriteFilePath,resultData,error=>{
            if(error){
                console.log("Error occur during writing data to json file",error);
            }else{
                console.log("Result written to the file successfully");
            }
        });
    };
};

bestconomyBowlerInSuperOvers();