//Matches per year
fetch('./output/1-matches-per-year.json')
.then((data)=>{
    return data.json();
})
.then((data)=>{
    
    let matchesSeason = Object.keys(data);
    let numberOfMatches =  Object.values(data);

    const chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container1',
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 0,
          beta: 3,
          depth: 100,
          viewDistance: 25
        }
      },
      xAxis: {
        title:{
          text:'IPL SEASONS'
        }
        ,
        categories: matchesSeason
      },
      yAxis: {
        title: {
          text:'MATCHES PLAYED'
        }
      },
      tooltip: {
        headerFormat: '<b>{point.key}</b><br>',
        pointFormat: 'Matches Played: {point.y}'
      },
      title: {
        text: '1. IPL MATCHES PER YEAR',
        align: 'left'
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        column: {
          depth: 25
        }
      },
      series: [{
        data: numberOfMatches,
        colorByPoint: true
      }]
    });
    
    function showValues() {
      document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
      document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
      document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
    }

    showValues();
})
.catch((error)=>{ 
});

//Matches won per team per year
fetch('./output/2-matches-won-per-team-per-year.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{

  let seasonList = [];
  Object.values(data).map((currentData)=>{
    Object.keys(currentData).map((season)=>{
      if(seasonList.indexOf(season) === -1){
        seasonList.push(season);
      }
    });
  });
  seasonList.sort((firstSeason,secondSeason)=>{
    return firstSeason-secondSeason;
  })

  let allTeamWithWinRecord = [];
  let teamData = {};
  Object.entries(data).map((currentData)=>{
    teamData['name'] = currentData[0];
    let highestAwardSeasonWise = [];
    seasonList.map((season)=>{
      if(currentData[1][season] === undefined){
        highestAwardSeasonWise.push("not played");
      }else{
        highestAwardSeasonWise.push(currentData[1][season]);
      }
    });
    teamData['data'] = highestAwardSeasonWise;
    allTeamWithWinRecord.push(teamData);
    teamData =  {};
  });
  Highcharts.chart('container2', {
    chart: {
      type: 'bar'
    },
    title: {
      text: '2. MATCHES WON PER TEAM PER YEAR',
      align: 'left'
    },
    xAxis: {
      categories: seasonList,
      title: {
        text: 'IPL SEASONS'
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'NUMBER OF MATCHES WON',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ''
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: allTeamWithWinRecord
  });
})
.catch((error)=>{
});

//Extra runs conceded per team in the year 2016
fetch('./output/3-extra-runs-conceded-per-team-2016.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{

  let teamList = Object.keys(data);
  let extraRunsConced = Object.values(data);

  const chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container3',
      type: 'bar',
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 3,
        depth: 100,
        viewDistance: 25
      }
    },
    xAxis: {
      title:{
        text:'IPL TEAMS'
      }
      ,
      categories: teamList
    },
    yAxis: {
      title: {
        text:'EXTRA RUNS CONCEDED'
      }
    },
    tooltip: {
      headerFormat: '<b>{point.key}</b><br>',
      pointFormat: 'Extra runs: {point.y}'
    },
    title: {
      text: '3. EXTRA RUNS CONCEDED PER TEAM IN THE YEAR 2016',
      align: 'left'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      column: {
        depth: 25
      }
    },
    series: [{
      data: extraRunsConced,
      colorByPoint: true
    }]
  });
  
  function showValues() {
    document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
    document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
    document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
  }
  
  showValues();
})
.catch((error)=>{
})

//Top 10 economical bowlers in the year 2015
fetch('./output/4-top-10-economical-bowlers-2015.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{
  
  let bowlerList = Object.keys(data);
  let bowlersEconomy = [];
  Object.values(data).map((bowler)=>{
    bowlersEconomy.push(parseFloat(bowler.economy));
  });

  const chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container4',
      type: 'column',
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 3,
        depth: 100,
        viewDistance: 25
      }
    },
    xAxis: {
      title:{
        text:'BOWLERS'
      }
      ,
      categories: bowlerList
    },
    yAxis: {
      title: {
        text:'ECONOMY RATES'
      }
    },
    tooltip: {
      headerFormat: '<b>{point.key}</b><br>',
      pointFormat: 'Economy rate: {point.y}'
    },
    title: {
      text: '4. TOP 10 ECONOMICAL BOWLERS IN THE YEAR 2015',
      align: 'left'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      column: {
        depth: 25
      }
    },
    series: [{
      data: bowlersEconomy,
      colorByPoint: true
    }]
  });
  
  function showValues() {
    document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
    document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
    document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
  }
  
  showValues();
})
.catch((error)=>{
});

//Team won toss won the match
fetch('./output/5-team-won-toss-won-match.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{

  let teamList = Object.keys(data);
  let wonTossWonMatchTimes = Object.values(data);

  const chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container5',
      type: 'bar',
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 3,
        depth: 100,
        viewDistance: 25
      }
    },
    xAxis: {
      title:{
        text:'IPL TEAMS'
      }
      ,
      categories: teamList
    },
    yAxis: {
      title: {
        text:'WON TOSS OWN MATCH TIMES'
      }
    },
    tooltip: {
      headerFormat: '<b>{point.key}</b><br>',
      pointFormat: 'Times: {point.y}'
    },
    title: {
      text: '5. TEAM WON TOSS WON MATCH',
      align: 'left'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      column: {
        depth: 25
      }
    },
    series: [{
      data: wonTossWonMatchTimes,
      colorByPoint: true
    }]
  });
  
  function showValues() {
    document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
    document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
    document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
  }
  
  showValues();
})
.catch((error)=>{
});

//Highest player of the match awards per season
fetch('./output/6-highest-player-of-match-each-season.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{

  let iplSeasons = Object.keys(data);
  
  let allPlayerOfTheMatchData = [];
  let playerOfMatchData = {};
  Object.entries(data).map((currentData)=>{
    playerOfMatchData['name'] = currentData[1].player;
    let highestAwardSeasonWise = [];
    iplSeasons.map((season)=>{
      if(currentData[0]==season){
        highestAwardSeasonWise.push(currentData[1].times_of_player_of_match);
      }else{
        highestAwardSeasonWise.push(" ");
      }
    });
    playerOfMatchData['data'] = highestAwardSeasonWise;
    allPlayerOfTheMatchData.push(playerOfMatchData);
    playerOfMatchData={};
  });

  Highcharts.chart('container6', {
    chart: {
      type: 'column'
    },
    title: {
      text: '6. HIGHEST PLAYER OF MATCH AWARDS PER SEASON',
      align: 'left'
    },
    xAxis: {
      categories: iplSeasons,
      title: {
        text: 'IPL SEASONS'
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'NUMBER OF PLAYER OF MATCH AWARDS',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ''
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: allPlayerOfTheMatchData
  });
})
.catch((error)=>{
})


//strike rate of batsman each season
fetch('./output/7-strike-rate-of-batsman-each-season.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{

  let allBatsmanWithStrikeRate = [];
  let iplSeasons = [];
  let batsmanData = {};
  Object.entries(data).map((currentData)=>{
    Object.keys(currentData[1]).map((season)=>{
      if(iplSeasons.indexOf(season)===-1){
        iplSeasons.push(season);
      }
    });
  });
  iplSeasons.sort((firstSeason,secondSeason)=>{
    return firstSeason-secondSeason;
  });
  Object.entries(data).map((currentData)=>{
    batsmanData['name'] = currentData[0];
    let strikeRate = [];
    iplSeasons.map((season)=>{
      if(currentData[1][season]=== false){
        strikeRate.push(" ");
      }else{
        strikeRate.push(Number(currentData[1][season]));
      }
    });

    batsmanData['data'] = strikeRate;
    allBatsmanWithStrikeRate.push(batsmanData);
    batsmanData = {};
  });

  Highcharts.chart('container7', {
    chart: {
      type: 'column'
    },
    title: {
      text: '7. STRIKE RATE OF BATSMAN EACH SEASON',
      align: 'left'
    },
    xAxis: {
      categories: iplSeasons,
      title: {
        text: 'IPL SEASONS'
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'STRIKE RATE',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ''
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      layout: 'below',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: allBatsmanWithStrikeRate
  });
})
.catch((error)=>{
});

//Highest times one player dismissed by another player
fetch('./output/8-highest-times-one-player-dismmised-by-another-player.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{

  let batsmanBowlerPair = `BATSMAN : ${data.batsman}  BOWLER : ${data.bowler}`;
  let timesOfDismissal = data.times_of_dismissal;

  const chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container8',
      type: 'column',
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 3,
        depth: 80,
        viewDistance: 100
      }
    },
    xAxis: {
      title:{
        text:'BATSMAN & BOWLER'
      }
      ,
      categories: [batsmanBowlerPair]
    },
    yAxis: {
      title: {
        text:'DISMISSAL TIMES'
      }
    },
    tooltip: {
      headerFormat: '<b>{point.key}</b><br>',
      pointFormat: 'DISMISSAL TIMES: {point.y}'
    },
    title: {
      text: '8. HIGHEST TIMES ONE PLAYER DISMISSED BY ANOTHER PLAYER',
      align: 'left'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      column: {
        depth: 25
      }
    },
    series: [{
      data: [timesOfDismissal],
      colorByPoint: true
    }]
  });
  
  function showValues() {
    document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
    document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
    document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
  }
  
  showValues();
})
.catch((error)=>{
});

//Best economy bowler in super overs
fetch('./output/9-best-economy-bowler-in-super-overs.json')
.then((data)=>{
  return data.json();
})
.then((data)=>{
  
  let bowler = data[0];
  let economy = Number(data[1].economy);

  const chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container9',
      type: 'column',
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 3,
        depth: 80,
        viewDistance: 100
      }
    },
    xAxis: {
      title:{
        text:'BOWLER'
      }
      ,
      categories: [bowler]
    },
    yAxis: {
      title: {
        text:'ECONOMY RATE'
      }
    },
    tooltip: {
      headerFormat: '<b>{point.key}</b><br>',
      pointFormat: 'ECONOMY RATE: {point.y}'
    },
    title: {
      text: '9. BEST ECONOMY BOWLER IN SUPER OVERS',
      align: 'left'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      column: {
        depth: 25
      }
    },
    series: [{
      data: [economy],
      colorByPoint: true
    }]
  });
  
  function showValues() {
    document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
    document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
    document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
  }
  
  showValues();
})
.catch((error)=>{
});